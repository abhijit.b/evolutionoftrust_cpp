#ifndef EVOLUTIONOFTRUST_ACTIONS_H
#define EVOLUTIONOFTRUST_ACTIONS_H


namespace EvolutionOfTrust {

enum Actions {
    COOPERATE,
    CHEAT
};

} // EvolutionOfTrust

#endif //EVOLUTIONOFTRUST_ACTIONS_H
