#include <memory>
#include <iostream>

#include "Game.h"

namespace EvolutionOfTrust {

    Game::Game(const std::string &player1Name,
               IStrategy *player1Strategy,
               const std::string &player2Name,
               IStrategy *player2Strategy,
               const int rounds,
               ScoreRules &&scoreRules)
            : rounds_(rounds),
              scoreRules_(std::move(scoreRules)),
              playerDetails_{
                      PlayerDetails(
                              new Player(player1Name, player1Strategy),
                              std::bind(&IStrategy::onAct, player1Strategy, std::placeholders::_1),
                              0
                      ),
                      PlayerDetails(
                              new Player(player2Name, player2Strategy),
                              std::bind(&IStrategy::onAct, player2Strategy, std::placeholders::_1),
                              0
                      )
              } {
    }

    Result Game::playRound() {
        Player &player1 = *std::get<0>(playerDetails_).getPlayer();
        Player &player2 = *std::get<1>(playerDetails_).getPlayer();
        Actions player1Act = player1.act();
        Actions player2Act = player2.act();

        std::get<0>(playerDetails_).getCallback()(player2Act);
        std::get<1>(playerDetails_).getCallback()(player1Act);

        return scoreRules_.evaluate(player1Act, player2Act);
    }

    Result Game::play() {
        for(int i = 0; i < rounds_; ++i) {
            Result result = std::move(playRound());
            std::get<0>(playerDetails_).getScore() += std::get<0>(result);
            std::get<1>(playerDetails_).getScore() += std::get<1>(result);

            std::cout << "After round " << i + 1 << " scores are: " << std::endl
            << "Player[ " << std::get<0>(playerDetails_).getPlayer()->getName() << "]: " << std::get<0>(playerDetails_).getScore() << std::endl
            << "Player[ " << std::get<1>(playerDetails_).getPlayer()->getName() << "]: " << std::get<1>(playerDetails_).getScore() << std::endl;
        }

        return std::make_tuple(std::get<0>(playerDetails_).getScore(), std::get<1>(playerDetails_).getScore());
    }

} // EvolutionOfTrust