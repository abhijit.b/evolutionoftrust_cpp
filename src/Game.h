#include <utility>

#ifndef SRC_GAME_H
#define SRC_GAME_H

#include <functional>

#include "Player.h"
#include "ScoreRules.h"

namespace EvolutionOfTrust {

    using namespace Strategy;

    using Result = ScoreRules::ScoreResult;

    class Game {
        class PlayerDetails {
            std::shared_ptr<Player> player_;
            std::function<void(Actions action)> callback_;
            int score_;

        public:
            PlayerDetails(Player *player, std::function<void(Actions)> callback,
                          int score) : player_(player), callback_(std::move(callback)), score_(score) {}

            std::shared_ptr<Player> getPlayer() { return player_; }
            std::function<void(Actions action)>& getCallback() { return callback_; }
            int& getScore() { return score_; }
        };


    public:
        Game(const std::string &player1Name, IStrategy *player1Strategy, const std::string &player2Name,
             IStrategy *player2Strategy, int rounds, ScoreRules &&scoreRules);

        ScoreRules::ScoreResult playRound();

        Result play();

    private:
        const int rounds_;
        std::tuple<PlayerDetails, PlayerDetails> playerDetails_;
        const ScoreRules scoreRules_;

    };

} // EvolutionOfTrust

#endif //SRC_GAME_H
