#ifndef EVOLUTIONOFTRUST_SCORERULES_H
#define EVOLUTIONOFTRUST_SCORERULES_H

#include <map>

#include "Actions.h"


namespace EvolutionOfTrust {

    constexpr short SCORE_CHEAT_CHEATED = 0;
    constexpr short SCORE_COOP_CHEATED = -1;
    constexpr short SCORE_COOP_COOPERATED = 2;
    constexpr short SCORE_CHEAT_COOPERATED = 3;


    class ScoreRules {
    public:
        using ScoreResult = std::tuple<const int, const int>;
    private:
        using ActionCombination = std::tuple<Actions, Actions>;

    public:
        ScoreRules();
        ScoreResult evaluate(Actions action1, Actions action2) const;

    private:
        void setupRules();

    private:
        std::map<ActionCombination, ScoreResult> rulesMap_;
    };

} // EvolutionOfTrust

#endif //EVOLUTIONOFTRUST_SCORERULES_H
