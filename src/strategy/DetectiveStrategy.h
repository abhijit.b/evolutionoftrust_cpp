#ifndef EVOLUTIONOFTRUST_DETECTIVESTRATEGY_H
#define EVOLUTIONOFTRUST_DETECTIVESTRATEGY_H

#include "IStrategy.h"

#include <deque>

namespace EvolutionOfTrust::Strategy {

    class DetectiveStrategy : public IStrategy {
        std::deque<Actions> firstMoves_;
        std::unique_ptr<IStrategy> currentStrategy_;

    public:
        DetectiveStrategy();

        Actions act() override;

        void onAct(Actions act) override;

    };

} // EvolutionOfTrust::Strategy

#endif //EVOLUTIONOFTRUST_DETECTIVESTRATEGY_H
