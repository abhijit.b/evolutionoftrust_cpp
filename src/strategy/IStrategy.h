#ifndef EVOLUTIONOFTRUST_ISTRATEGY_H
#define EVOLUTIONOFTRUST_ISTRATEGY_H

#include <Actions.h>

namespace EvolutionOfTrust::Strategy {

    class IStrategy   {
    public:
        virtual ~IStrategy() = default;

        virtual Actions act() = 0;
        virtual void onAct(Actions act) {}
    };

} // EvolutionOfTrust::Strategy

#endif //EVOLUTIONOFTRUST_ISTRATEGY_H
