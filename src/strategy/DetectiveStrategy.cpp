#include <memory>

#include "DetectiveStrategy.h"
#include "AlwaysStrategy.h"
#include "CopycatStrategy.h"


namespace EvolutionOfTrust::Strategy {

    DetectiveStrategy::DetectiveStrategy() :
        firstMoves_{Actions::COOPERATE, Actions::COOPERATE, Actions::CHEAT, Actions::COOPERATE},
        currentStrategy_(){}

    Actions DetectiveStrategy::act() {
        if(currentStrategy_)
            return currentStrategy_->act();

        if(!firstMoves_.empty()) {
            Actions action = firstMoves_.front();
            firstMoves_.pop_front();
            return action;
        }

        return Actions::CHEAT;
    }

    void DetectiveStrategy::onAct(Actions act) {
        if(!currentStrategy_) {
            if(act == Actions::CHEAT)
                currentStrategy_ = std::make_unique<CopycatStrategy>();
        }

        if(currentStrategy_)
            currentStrategy_->onAct(act);
    }

} // EvolutionOfTrust::Strategy