#include <memory>

#include "GrudgeStrategy.h"


namespace EvolutionOfTrust::Strategy {

    Actions GrudgeStrategy::act() {
        return currentStrategy_ ? currentStrategy_->act() : Actions::COOPERATE;
    }

    void GrudgeStrategy::onAct(Actions act) {
        if (!currentStrategy_ && act == Actions::CHEAT)
            currentStrategy_ = std::make_unique<AlwaysCheatStrategy>();
    }

} // EvolutionOfTrust::Strategy
