#ifndef EVOLUTIONOFTRUST_GRUDGESTRATEGY_H
#define EVOLUTIONOFTRUST_GRUDGESTRATEGY_H

#include "IStrategy.h"
#include "AlwaysStrategy.h"

#include <memory>

namespace EvolutionOfTrust::Strategy {

        class GrudgeStrategy : public IStrategy {
        public:
            Actions act() override;

            void onAct(Actions act) override;

        private:
            std::unique_ptr<IStrategy> currentStrategy_;
        };

    } // EvolutionOfTrust::Strategy

#endif //EVOLUTIONOFTRUST_GRUDGESTRATEGY_H
