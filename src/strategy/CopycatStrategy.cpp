#include "CopycatStrategy.h"

#include "Game.h"

namespace EvolutionOfTrust::Strategy {

    Actions CopycatStrategy::act() {
        return action_;
    }

    void CopycatStrategy::onAct(Actions act) {
        action_ = act;
    }

} // EvolutionOfTrust::Strategy