#ifndef EVOLUTIONOFTRUST_CONSOLESTRATEGY_H
#define EVOLUTIONOFTRUST_CONSOLESTRATEGY_H

#include <iostream>
#include "IStrategy.h"

namespace EvolutionOfTrust::Strategy {

    class ConsoleStrategy : public IStrategy {
    public:
        Actions act() override;

        void onAct(Actions act) override;
    };

} // EvolutionOfTrust::Strategy


#endif //EVOLUTIONOFTRUST_CONSOLESTRATEGY_H
