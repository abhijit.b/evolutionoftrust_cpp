#ifndef EVOLUTIONOFTRUST_ALWAYSSTRATEGY_H
#define EVOLUTIONOFTRUST_ALWAYSSTRATEGY_H

#include "IStrategy.h"

namespace EvolutionOfTrust::Strategy {

    class AlwaysCheatStrategy : public IStrategy {
    public:
        Actions act() override {
            return Actions::CHEAT;
        }
    };

    class AlwaysCooperateStrategy : public IStrategy {
    public:
        Actions act() override {
            return Actions::COOPERATE;
        }
    };

}  // EvolutionOfTrust::Strategy

#endif //EVOLUTIONOFTRUST_ALWAYSSTRATEGY_H
