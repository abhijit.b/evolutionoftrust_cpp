#include "ConsoleStrategy.h"

#include <string>

namespace EvolutionOfTrust::Strategy {

    Actions ConsoleStrategy::act() {
        std::string input;

        std::cout << "Select your move:\n\t[1]\tCOOPERATE(default)\n\t[2]\tCHEAT: ";
        std::cin >> input;

        if(input == "2")
            return Actions::CHEAT;
        return Actions::COOPERATE;

    }

    void ConsoleStrategy::onAct(Actions act) {
        std::cout << "Your opponent has decided to " << (act == Actions::COOPERATE ? "cooperate" : "cheat");
    }
} // EvolutionOfTrust::Strategy

