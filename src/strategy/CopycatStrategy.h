#ifndef EVOLUTIONOFTRUST_COPYCATSTRATEGY_H
#define EVOLUTIONOFTRUST_COPYCATSTRATEGY_H

#include <optional>
#include <memory>

#include "IStrategy.h"


namespace EvolutionOfTrust::Strategy {

    class CopycatStrategy : public IStrategy {
    public:
        Actions act() override;

        void onAct(Actions act) override;

    private:
        Actions action_ = Actions::COOPERATE;
    };

} // EvolutionOfTrust::Strategy

#endif //EVOLUTIONOFTRUST_COPYCATSTRATEGY_H
