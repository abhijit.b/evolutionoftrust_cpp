#include "Player.h"

namespace EvolutionOfTrust {

    Player::Player(std::string name, Strategy::IStrategy* strategy)
            : name_(std::move(name)), strategy_(strategy) {}

    Actions Player::act() {
        return strategy_->act();
    }

    bool Player::operator<(const EvolutionOfTrust::Player &other) const {
        return name_ < other.name_;
    }


    std::shared_ptr<Strategy::IStrategy> Player::getStrategy() { return strategy_; }

    const std::string Player::getName() {
        return name_;
    }

} // EvolutionOfTrust