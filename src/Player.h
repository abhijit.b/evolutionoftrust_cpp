#ifndef EVOLUTIONOFTRUST_PLAYER_H
#define EVOLUTIONOFTRUST_PLAYER_H

#include <string>
#include <memory>

#include <strategy/IStrategy.h>
#include "Actions.h"

namespace EvolutionOfTrust {

    class Player {
    private:
        const std::string name_;
        std::shared_ptr<Strategy::IStrategy> strategy_;

    public:
        Player(std::string name, Strategy::IStrategy* strategy);

        std::shared_ptr<Strategy::IStrategy> getStrategy();
        Actions act();

        const std::string getName();
        bool operator<(const Player& other) const;
    };

} // EvolutionOfTrust


#endif //EVOLUTIONOFTRUST_PLAYER_H
