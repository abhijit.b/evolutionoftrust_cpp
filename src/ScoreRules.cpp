#include "ScoreRules.h"
#include <exception>

namespace EvolutionOfTrust {

    ScoreRules::ScoreRules() {
        setupRules();
    }


    void ScoreRules::setupRules() {
        rulesMap_.insert(std::make_pair(std::make_tuple(Actions::CHEAT, Actions::CHEAT),
                                        std::make_tuple(SCORE_CHEAT_CHEATED, SCORE_CHEAT_CHEATED)));
        rulesMap_.insert(std::make_pair(std::make_tuple(Actions::COOPERATE, Actions::CHEAT),
                                        std::make_tuple(SCORE_COOP_CHEATED, SCORE_CHEAT_COOPERATED)));
        rulesMap_.insert(std::make_pair(std::make_tuple(Actions::CHEAT, Actions::COOPERATE),
                                        std::make_tuple(SCORE_CHEAT_COOPERATED, SCORE_COOP_CHEATED)));
        rulesMap_.insert(std::make_pair(std::make_tuple(Actions::COOPERATE, Actions::COOPERATE),
                                        std::make_tuple(SCORE_COOP_COOPERATED, SCORE_COOP_COOPERATED)));
    }

    ScoreRules::ScoreResult
    ScoreRules::evaluate(Actions action1, Actions action2) const {
        auto result = rulesMap_.find(std::make_tuple(action1, action2));
        if (result == rulesMap_.end())
            throw std::exception();
        return (*result).second;
    }

}
