#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <strategy/ConsoleStrategy.h>
#include <strategy/AlwaysStrategy.h>
#include "Game.h"


using namespace EvolutionOfTrust;
using namespace EvolutionOfTrust::Strategy;

using ::testing::AtLeast;
using ::testing::Return;

class MockStrategy : public IStrategy {
public:
    MOCK_METHOD0(act, Actions());
    MOCK_METHOD1(onAct, void(Actions));
};


TEST(Test_Game, scoreShouldBe_10_10_ForBothCooperators_PlayingFor_5_Rounds) {
    auto player1CooperateStrategy = new MockStrategy();
    EXPECT_CALL(*player1CooperateStrategy, act()).Times(5).WillRepeatedly(Return(Actions::COOPERATE));
    auto player2CooperateStrategy = new MockStrategy();
    EXPECT_CALL(*player2CooperateStrategy, act()).Times(5).WillRepeatedly(Return(Actions::COOPERATE));

    Game game("Coop1", player1CooperateStrategy, "Coop2", player2CooperateStrategy, 5, ScoreRules());
    Result result = game.play();

    EXPECT_EQ(std::make_tuple(10, 10), result);
}

TEST(Test_Game, scoreShouldBe_0_0_ForBothCheaters_PlayingFor_5_Rounds) {
    auto player1CheatStrategy = new MockStrategy();
    EXPECT_CALL(*player1CheatStrategy, act()).Times(5).WillRepeatedly(Return(Actions::CHEAT));
    auto player2CheatStrategy = new MockStrategy();
    EXPECT_CALL(*player2CheatStrategy, act()).Times(5).WillRepeatedly(Return(Actions::CHEAT));

    Game game("Cheater1", player1CheatStrategy, "Cheater2", player2CheatStrategy, 5, ScoreRules());
    Result result = game.play();

    EXPECT_EQ(std::make_tuple(0, 0), result);
}

TEST(Test_Game, scoreShouldBe_Minus5_15_ForCooperatorCheater_PlayingFor_5_Rounds) {
    auto player1CoopStrategy = new MockStrategy();
    EXPECT_CALL(*player1CoopStrategy, act()).Times(5).WillRepeatedly(Return(Actions::COOPERATE));
    auto player2CheatStrategy = new MockStrategy();
    EXPECT_CALL(*player2CheatStrategy, act()).Times(5).WillRepeatedly(Return(Actions::CHEAT));

    Game game("Coop1", player1CoopStrategy, "Cheater2", player2CheatStrategy, 5, ScoreRules());
    Result result = game.play();

    EXPECT_EQ(std::make_tuple(-5, 15), result);
}

TEST(Test_Game, scoreShouldBe_Minus1_3_ForCopycatCheater_PlayingFor_5_Rounds) {
    auto player1CopycatStrategy = new MockStrategy();
    EXPECT_CALL(*player1CopycatStrategy, act()).Times(5).WillOnce(Return(Actions::COOPERATE)).WillRepeatedly(Return(Actions::CHEAT));
    auto player2CheatStrategy = new MockStrategy();
    EXPECT_CALL(*player2CheatStrategy, act()).Times(5).WillRepeatedly(Return(Actions::CHEAT));

    Game game("Copycat1", player1CopycatStrategy, "Cheater2", player2CheatStrategy, 5, ScoreRules());
    Result result = game.play();

    EXPECT_EQ(std::make_tuple(-1, 3), result);
}

TEST(Test_Game, scoreShouldBe_Minus3_9_ForDetectiveCheater_PlayingFor_5_Rounds) {
    auto player1DetectiveStrategy = new MockStrategy();
    EXPECT_CALL(*player1DetectiveStrategy, act()).Times(5)
    .WillOnce(Return(Actions::COOPERATE))
    .WillOnce(Return(Actions::COOPERATE))
    .WillOnce(Return(Actions::CHEAT))
    .WillOnce(Return(Actions::COOPERATE))
    .WillRepeatedly(Return(Actions::CHEAT));
    auto player2CheatStrategy = new MockStrategy();
    EXPECT_CALL(*player2CheatStrategy, act()).Times(5).WillRepeatedly(Return(Actions::CHEAT));

    Game game("Detective1", player1DetectiveStrategy, "Cheater2", player2CheatStrategy, 5, ScoreRules());
    Result result = game.play();

    EXPECT_EQ(std::make_tuple(-3, 9), result);
}


TEST(Test_Game, scoreShouldBe_12_4_ForDetectiveCoop_PlayingFor_5_Rounds) {
    auto player1DetectiveStrategy = new MockStrategy();
    EXPECT_CALL(*player1DetectiveStrategy, act()).Times(5)
    .WillOnce(Return(Actions::COOPERATE))
    .WillOnce(Return(Actions::COOPERATE))
    .WillOnce(Return(Actions::CHEAT))
    .WillOnce(Return(Actions::COOPERATE))
    .WillRepeatedly(Return(Actions::CHEAT));
    auto player2CoopStrategy = new MockStrategy();
    EXPECT_CALL(*player2CoopStrategy, act()).Times(5).WillRepeatedly(Return(Actions::COOPERATE));

    Game game("Detective1", player1DetectiveStrategy, "Coop2", player2CoopStrategy, 5, ScoreRules());
    Result result = game.play();

    EXPECT_EQ(12, std::get<0>(result));
    EXPECT_EQ(4, std::get<1>(result));
}


TEST(Test_Game, scoreShouldBe_6_6_ForDetectiveGrudge_PlayingFor_5_Rounds) {
    auto player1DetectiveStrategy = new MockStrategy();
    EXPECT_CALL(*player1DetectiveStrategy, act()).Times(5)
    .WillOnce(Return(Actions::COOPERATE))
    .WillOnce(Return(Actions::COOPERATE))
    .WillOnce(Return(Actions::CHEAT))
    .WillOnce(Return(Actions::COOPERATE))
    .WillRepeatedly(Return(Actions::CHEAT));
    auto player2GrudgeStrategy = new MockStrategy();
    EXPECT_CALL(*player2GrudgeStrategy, act()).Times(5)
    .WillOnce(Return(Actions::COOPERATE))
    .WillOnce(Return(Actions::COOPERATE))
    .WillOnce(Return(Actions::COOPERATE))
    .WillRepeatedly(Return(Actions::CHEAT));

    Game game("Detective1", player1DetectiveStrategy, "Coop2", player2GrudgeStrategy, 5, ScoreRules());
    Result result = game.play();

    EXPECT_EQ(6, std::get<0>(result));
    EXPECT_EQ(6, std::get<1>(result));
}


