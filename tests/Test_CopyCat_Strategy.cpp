#include <gtest/gtest.h>
#include <strategy/AlwaysStrategy.h>
#include <strategy/CopycatStrategy.h>
#include "Game.h"

using namespace EvolutionOfTrust;
using namespace EvolutionOfTrust::Strategy;

TEST(Test_CopyCat_Strategy, shouldMatchPreviousMovesReceivedAsInputByCopycat) {
    CopycatStrategy *copycatStrategy = new CopycatStrategy();

    std::vector<Actions> moves{Actions::CHEAT, Actions::COOPERATE, Actions::CHEAT};

    std::for_each(
            std::begin(moves), std::end(moves),
            [copycatStrategy](Actions act) {
                copycatStrategy->onAct(act);
                EXPECT_EQ(act, copycatStrategy->act());
            }
    );
}

