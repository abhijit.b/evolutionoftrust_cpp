#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <strategy/ConsoleStrategy.h>
#include <strategy/AlwaysStrategy.h>
#include "Game.h"


using namespace EvolutionOfTrust;
using namespace EvolutionOfTrust::Strategy;

using ::testing::AtLeast;
using ::testing::Return;

class MockConsoleStrategy : public ConsoleStrategy {
public:
    MOCK_METHOD0(act, Actions());
    MOCK_METHOD1(onAct, void(Actions));
};


TEST(Test_Console_Strategy, verifyFlowForConsoleStrategyThroughMock) {
    auto mockConsoleStrategy = new MockConsoleStrategy();
    EXPECT_CALL(*mockConsoleStrategy, act()).Times(1).WillOnce(Return(Actions::COOPERATE));
    EXPECT_CALL(*mockConsoleStrategy, onAct(Actions::CHEAT)).Times(AtLeast(1));

    Game game("Cheater", new AlwaysCheatStrategy(), "Console Player", mockConsoleStrategy, 0, ScoreRules());
    Result result = game.playRound();
    EXPECT_EQ(SCORE_COOP_CHEATED, std::get<1>(result));
}
