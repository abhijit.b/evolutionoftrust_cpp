#include <gtest/gtest.h>
#include <strategy/AlwaysStrategy.h>
#include <strategy/DetectiveStrategy.h>
#include "Game.h"

using namespace EvolutionOfTrust;
using namespace EvolutionOfTrust::Strategy;

TEST(Test_Detective_Strategy, verifyFirstFourMovesForDetectiveStrategy) {
    auto detectiveStrategy = new DetectiveStrategy();
    EXPECT_EQ(Actions::COOPERATE, detectiveStrategy->act());
    EXPECT_EQ(Actions::COOPERATE, detectiveStrategy->act());
    EXPECT_EQ(Actions::CHEAT, detectiveStrategy->act());
    EXPECT_EQ(Actions::COOPERATE, detectiveStrategy->act());
}

TEST(Test_Detective_Strategy, verifyCopycatStrategyWhenDetectiveDetectsCheating) {
    auto detectiveStrategy = new DetectiveStrategy();
    EXPECT_EQ(Actions::COOPERATE, detectiveStrategy->act());
    EXPECT_EQ(Actions::COOPERATE, detectiveStrategy->act());
    EXPECT_EQ(Actions::CHEAT, detectiveStrategy->act());
    EXPECT_EQ(Actions::COOPERATE, detectiveStrategy->act());

    detectiveStrategy->onAct(Actions::COOPERATE);
    EXPECT_EQ(Actions::CHEAT, detectiveStrategy->act());
    detectiveStrategy->onAct(Actions::CHEAT);
    EXPECT_EQ(Actions::CHEAT, detectiveStrategy->act());
    detectiveStrategy->onAct(Actions::COOPERATE);
    EXPECT_EQ(Actions::COOPERATE, detectiveStrategy->act());
}
