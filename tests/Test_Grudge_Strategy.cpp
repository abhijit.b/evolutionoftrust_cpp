#include <gtest/gtest.h>
#include <strategy/AlwaysStrategy.h>
#include <strategy/GrudgeStrategy.h>
#include "Game.h"

using namespace EvolutionOfTrust;
using namespace EvolutionOfTrust::Strategy;

TEST(Test_Grudge_Strategy, shouldAlwaysCheatAfterCheated) {
    auto grudgeStrategy = new GrudgeStrategy();
    EXPECT_EQ(Actions::COOPERATE, grudgeStrategy->act());
    grudgeStrategy->onAct(Actions::COOPERATE);
    EXPECT_EQ(Actions::COOPERATE, grudgeStrategy->act());
    grudgeStrategy->onAct(Actions::CHEAT);
    EXPECT_EQ(Actions::CHEAT, grudgeStrategy->act());
    grudgeStrategy->onAct(Actions::COOPERATE);
    EXPECT_EQ(Actions::CHEAT, grudgeStrategy->act());
}

