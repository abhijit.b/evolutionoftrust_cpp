#include <gtest/gtest.h>
#include <strategy/AlwaysStrategy.h>
#include "Game.h"

using namespace EvolutionOfTrust;
using namespace EvolutionOfTrust::Strategy;

TEST(Test_PlayRules, shouldReturn_00_WhenBothCheated) {
    Game game("Cheater1",
              new AlwaysCheatStrategy(),
              "Cheater2",
              new AlwaysCheatStrategy(),
              0,
              ScoreRules());

    ScoreRules::ScoreResult result = game.playRound();

    EXPECT_EQ(std::make_tuple(SCORE_CHEAT_CHEATED, SCORE_CHEAT_CHEATED), result);
}

TEST(Test_PlayRules, shouldReturn_22_WhenBothCooperated) {
    Game game("Cooperator1",
              new AlwaysCooperateStrategy(),
              "Cooperator1",
              new AlwaysCooperateStrategy(),
              0,
              ScoreRules());

    ScoreRules::ScoreResult result = game.playRound();
    EXPECT_EQ(std::make_tuple(SCORE_COOP_COOPERATED, SCORE_COOP_COOPERATED), result);
}

TEST(Test_PlayRules, shouldReturn_3Minus1_WhenBothCooperated) {
    Game game("Cheater",
              new AlwaysCheatStrategy(),
              "Cooperator",
              new AlwaysCooperateStrategy(),
              0,
              ScoreRules());

    ScoreRules::ScoreResult result = game.playRound();
    EXPECT_EQ(std::make_tuple(SCORE_CHEAT_COOPERATED, SCORE_COOP_CHEATED), result);
}