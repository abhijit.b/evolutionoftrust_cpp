#include <iostream>
#include <strategy/CopycatStrategy.h>
#include <strategy/AlwaysStrategy.h>
#include <strategy/GrudgeStrategy.h>
#include <strategy/DetectiveStrategy.h>
#include <strategy/ConsoleStrategy.h>

#include "Game.h"

using namespace EvolutionOfTrust;
using namespace EvolutionOfTrust::Strategy;

IStrategy* getStrategy(const std::string& selection) {
    IStrategy* strategy;
    if(selection == "1") {
        strategy = new AlwaysCheatStrategy();
    }
    else if(selection == "2") {
        strategy = new AlwaysCooperateStrategy();
    }
    else if(selection == "4") {
        strategy = new GrudgeStrategy();
    }
    else if(selection == "5") {
        strategy = new DetectiveStrategy();
    }
    else if(selection == "6") {
        strategy = new ConsoleStrategy();
    }
    else {
        strategy = new CopycatStrategy();
    }

    return strategy;
}

int main() {
    std::cout << "Evolution Of Trust" << std::endl;

    std::cout << "Select your opponent: " << std::endl
              << "1. Always Cheat" << std::endl
              << "2. Always Cooperate" << std::endl
              << "3. Copycat" << std::endl
              << "4. Grudger" << std::endl
              << "5. Detective" << std::endl
              << "6. Console" << std::endl;
    std::cout << "Your Selection (Defaults to Copycat if invalid selection is made): ";
    std::string input;
    std::cin >> input;

    IStrategy* strategy = getStrategy(input);

    Game theGame("You", new ConsoleStrategy(), "Opponent", strategy, 5, ScoreRules());
    theGame.play();

    return 0;
}